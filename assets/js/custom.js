(function($) {
  var toggle = document.getElementById("menu-toggle");
  var menu = document.getElementById("menu");
  var container = document.getElementById("background")
  
  
  toggle.addEventListener("click", function(e) {
    if (menu.classList.contains("open")) {
						container.setAttribute('class',null)
      menu.classList.remove("open");
    } else {
		container.setAttribute('class','blur')
      menu.classList.add("open");
    }
  });

	
  // Close menu after click on all screens
  $(window).on("resize", function() {
	  
      $(".main-menu a").on("click", function(e) {
        menu.classList.remove("open");
		container.setAttribute('class',null)
      });
	  
  });
	
container.addEventListener("click", function(){
	 if (menu.classList.contains("open")) {
	  	container.setAttribute('class',null)
      	menu.classList.remove("open");
    } else {
		//do nothing
	}
});

$(".main-menu a").on("click", function(e) {
	menu.classList.remove("open");
	container.setAttribute('class',null)
  });

  $(".owl-carousel").owlCarousel({
    items: 4,
    lazyLoad: true,
    loop: true,
    dots: true,
    margin: 30,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });
	

  $(".hover").mouseleave(function() {
    $(this).removeClass("hover");
  });

  $(".isotope-wrapper").each(function() {
    var $isotope = $(".isotope-box", this);
    var $filterCheckboxes = $('input[type="radio"]', this);

    var filter = function() {
      var type = $filterCheckboxes.filter(":checked").data("type") || "*";
      if (type !== "*") {
        type = '[data-type="' + type + '"]';
      }
      $isotope.isotope({ filter: type });
    };

    $isotope.isotope({
      itemSelector: ".isotope-item",
      layoutMode: "masonry"
    });

    $(this).on("change", filter);
    filter();
  });

  lightbox.option({
    resizeDuration: 200,
    wrapAround: true
  });
})(jQuery);
